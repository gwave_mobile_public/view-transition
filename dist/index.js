var __create = Object.create;
var __defProp = Object.defineProperty;
var __getOwnPropDesc = Object.getOwnPropertyDescriptor;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __getProtoOf = Object.getPrototypeOf;
var __hasOwnProp = Object.prototype.hasOwnProperty;
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};
var __copyProps = (to, from, except, desc) => {
  if (from && typeof from === "object" || typeof from === "function") {
    for (let key of __getOwnPropNames(from))
      if (!__hasOwnProp.call(to, key) && key !== except)
        __defProp(to, key, { get: () => from[key], enumerable: !(desc = __getOwnPropDesc(from, key)) || desc.enumerable });
  }
  return to;
};
var __toESM = (mod, isNodeMode, target) => (target = mod != null ? __create(__getProtoOf(mod)) : {}, __copyProps(
  // If the importer is in node compatibility mode or this is not an ESM
  // file that has been converted to a CommonJS file using a Babel-
  // compatible transform (i.e. "__esModule" has not been set), then set
  // "default" to the CommonJS "module.exports" for node compatibility.
  isNodeMode || !mod || !mod.__esModule ? __defProp(target, "default", { value: mod, enumerable: true }) : target,
  mod
));
var __toCommonJS = (mod) => __copyProps(__defProp({}, "__esModule", { value: true }), mod);

// index.ts
var view_transition_exports = {};
__export(view_transition_exports, {
  AnimatedLink: () => AnimatedLink,
  useViewTransition: () => useViewTransition,
  useViewTransitionRouter: () => useViewTransitionRouter
});
module.exports = __toCommonJS(view_transition_exports);

// src/animated-link/index.tsx
var import_react2 = __toESM(require("react"));

// src/hooks/useViewTransitionRouter.ts
var import_react = require("react");
var import_navigation = require("next/navigation");

// src/utils.ts
function viewTransition({
  skipTransition = false,
  classNames = [],
  updateDOM
}) {
  const extendedDocument = document;
  if (skipTransition || !extendedDocument.startViewTransition) {
    const updateCallbackDone = Promise.resolve(updateDOM()).then(() => {
    });
    const ready = Promise.reject(Error("View transitions unsupported"));
    return {
      ready,
      updateCallbackDone,
      finished: updateCallbackDone,
      skipTransition: () => {
      }
    };
  }
  extendedDocument.documentElement.classList.add(...classNames);
  const transition = extendedDocument.startViewTransition(updateDOM);
  transition.finished.finally(
    () => extendedDocument.documentElement.classList.remove(...classNames)
  );
  return transition;
}

// src/hooks/useViewTransitionRouter.ts
var useViewTransitionRouter = () => {
  const router = (0, import_navigation.useRouter)();
  const pathname = (0, import_navigation.usePathname)();
  const promiseCallbacks = (0, import_react.useRef)(null);
  (0, import_react.useLayoutEffect)(() => {
    return () => {
      if (promiseCallbacks.current) {
        promiseCallbacks.current.resolve(void 0);
        promiseCallbacks.current = null;
      }
    };
  }, [pathname]);
  return {
    ...router,
    // 使用浏览器onpopstate监听时，next/router back/forward不需要封装
    // back: () => {
    //   viewTransition({
    //     updateDOM: () => router.back()
    //   });
    // },
    // forward: () => {
    //   viewTransition({
    //     updateDOM: () => router.forward()
    //   });
    // },
    push: (...args) => {
      viewTransition({
        updateDOM: () => {
          const url = args[0];
          if (url === pathname) {
            router.push(...args);
          } else {
            return new Promise((resolve, reject) => {
              promiseCallbacks.current = { resolve, reject };
              router.push(...args);
            });
          }
        }
      });
    },
    replace: (...args) => {
      viewTransition({
        updateDOM: () => router.replace(...args)
      });
    }
  };
};

// src/animated-link/index.tsx
var import_link = __toESM(require("next/link"));
var AnimatedLink = ({ ...props }) => {
  const router = useViewTransitionRouter();
  const handleClick = async (e) => {
    e.preventDefault();
    router.push(props.href.toString());
  };
  return /* @__PURE__ */ import_react2.default.createElement(
    import_link.default,
    {
      ...props,
      onClick: handleClick
    }
  );
};

// src/hooks/useViewTransition.ts
var import_react3 = require("react");
var useViewTransition = () => {
  const [, startTransition] = (0, import_react3.useTransition)();
  const popStateViewTransition = (params) => {
    window.onpopstate = function() {
      startTransition(() => {
        viewTransition({
          skipTransition: params?.skipTransition,
          classNames: params?.classNames,
          updateDOM: () => {
          }
        });
      });
    };
  };
  return {
    popStateViewTransition
  };
};
// Annotate the CommonJS export names for ESM import in node:
0 && (module.exports = {
  AnimatedLink,
  useViewTransition,
  useViewTransitionRouter
});
