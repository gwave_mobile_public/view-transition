'use client';

interface ViewTransition {
  finished: Promise<void>;
  ready: Promise<void>;
  updateCallbackDone: Promise<void>;
}

interface ExtendedDocument extends Document {
  startViewTransition(setupPromise: () => Promise<void> | void): ViewTransition;
}

interface PropsType {
  skipTransition?: boolean,
  classNames?: string[],
  updateDOM: () => Promise<void> | void
}

export function viewTransition ({
  skipTransition = false,
  classNames = [],
  updateDOM
}: PropsType) {
  const extendedDocument = document as ExtendedDocument;
  if (skipTransition || !extendedDocument.startViewTransition) {
    const updateCallbackDone = Promise.resolve(updateDOM()).then(() => {});
    const ready = Promise.reject(Error('View transitions unsupported'));
    return {
      ready,
      updateCallbackDone,
      finished: updateCallbackDone,
      skipTransition: () => {}
    };
  }
  extendedDocument.documentElement.classList.add(...classNames);
  const transition = extendedDocument.startViewTransition(updateDOM);
  transition.finished.finally(() => 
    extendedDocument.documentElement.classList.remove(...classNames)
  );
  return transition;
}