'use client';
import { useLayoutEffect, useRef } from 'react';
import { useRouter, usePathname } from 'next/navigation';
import { viewTransition } from '../utils';

interface ViewTransition {
  finished: Promise<void>;
  ready: Promise<void>;
  updateCallbackDone: Promise<void>;
}

interface ExtendedDocument extends Document {
  startViewTransition(setupPromise: () => Promise<void> | void): ViewTransition;
}

export const useViewTransitionRouter = () => {
  const router = useRouter();
  const pathname = usePathname();

  const promiseCallbacks = useRef<Record<
    "resolve" | "reject",
    (value: unknown) => void
  > | null>(null);

  useLayoutEffect(() => {
    return () => {
      if (promiseCallbacks.current) {
        promiseCallbacks.current.resolve(undefined);
        promiseCallbacks.current = null;
      }
    };
  }, [pathname]);

  return {
    ...router,
    // 使用浏览器onpopstate监听时，next/router back/forward不需要封装
    // back: () => {
    //   viewTransition({
    //     updateDOM: () => router.back()
    //   });
    // },
    // forward: () => {
    //   viewTransition({
    //     updateDOM: () => router.forward()
    //   });
    // },
    push: (...args: Parameters<typeof router.push>) => {
      viewTransition({
        updateDOM: () => {
          const url = args[0] as string;
          if (url === pathname) {
            router.push(...args);
          } else {
            return new Promise((resolve, reject) => {
              promiseCallbacks.current = { resolve, reject };
              router.push(...args);
            });
          }
        }
      });
    },
    replace: (...args: Parameters<typeof router.replace>) => {
      viewTransition({
        updateDOM: () => router.replace(...args)
      });
    }
  }
};
