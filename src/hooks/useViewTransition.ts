'use client';
import { useTransition } from 'react';
import { viewTransition } from '../utils';

interface ParamsType {
  skipTransition?: boolean,
  classNames?: string[],
}

export const useViewTransition = () => {
  const [, startTransition] = useTransition();

  const popStateViewTransition = (params?: ParamsType) => {
    window.onpopstate = function() {
      startTransition(() => {
        viewTransition({
          skipTransition: params?.skipTransition,
          classNames: params?.classNames,
          updateDOM: ()=> {}});
      })
    }
  };

  return {
    popStateViewTransition
  };
};