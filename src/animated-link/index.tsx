'use client';
import React from 'react';
import { useViewTransitionRouter } from '../hooks/useViewTransitionRouter';
import Link from 'next/link';

interface LinkPropsType {
  children: React.ReactNode;
  href: string;
  [key: string]: any;
}

export const AnimatedLink: React.FC<LinkPropsType> = ({ ...props }) => {
  const router = useViewTransitionRouter();

  const handleClick = async (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    router.push(props.href.toString());
  };

  return (
    <Link
      { ...props }
      onClick={handleClick}     
    />
  );
};
