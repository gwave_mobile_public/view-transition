<!-- Intro-->

<!--
* Thanks for reviewing my Project-README-Template! 
* Access the blank-template here (https://github.com/YousefIbrahimismail/Project-README-Template/blob/main/Templates/_blank-README.md) 
* 
* Read the comments for an easy step by step guide.or read this Make_it_Yours guide here: () // add Personalization_md_file
* Enjoy!
-->


<!-- Shields Section--><!-- Optional -->

<!-- 
* Insert project shields and badges through this link https://shields.io/
* 
*
-->



<!-- Logo Section  --><!-- Required -->

<!--
* Insert an image URL in the <img> "src" attribute bellow. (line )
* 
* Insert your github profile URL in the <a> "href" attribute bellow (line )
-->


<!-- Logo Section  --> <!-- Required -->

<!--
* Insert your github profile URL in the <a> "href" attribute bellow (line-25)
* 
* Insert an image URL in the <img> "src" attribute bellow. (line-26)
-->
<div align="center">
    <a href="your_github_user_name" target="_blank">
        <img src="https://user-images.githubusercontent.com/59213365/198116794-365cd9b5-e705-4111-a249-85ed713b9c87.jpg" 
        alt="Logo" height="300" width="auto">
    </a>
</div>


<!-- Project title 
* use a dynamic typing-SvG here https://readme-typing-svg.demolab.com/demo/
*
*  Instead you can type your project name after a # header
-->

<div align="center">
<img src="https://readme-typing-svg.demolab.com?font=Fira+Code&size=22&duration=4000&pause=5000&background=FFFFFF00&center=true&vCenter=true&multiline=true&width=435&lines=Nextjs-View-Transition">
</div>






## About<!-- Required -->
<!-- 
* information about the project 
* 
* keep it short and sweet
-->


A cool view transition animation for nextjs <br/><br/>
此插件基于chrome [View Transitions API](https://developer.chrome.com/docs/web-platform/view-transitions/) 实现页面间转场动画，适用于使用next.js开发的应用，重新封装了next/link和next/navigation里的useRouter方法，并且针对浏览器的前进和后退状态进行监听处理，请自行选择配置。
- useViewTransitionRouter，主要针对useRouter里的push、replace等方法加了转场动画处理
- useViewTransition，对于浏览器状态进行监听，在点击前进\后退时添加转场动画
- AnimatedLink，给next/link的Link组件添加了转场动画

## How to use this project<!-- Required -->
<!-- 
* Here you may add information about how 
* 
* and why to use this project.
-->

- 在项目package.json里面导入

```bash
    "dependencies": {
        ...,
        "view-transition": "https://gitlab.com/gwave_mobile_public/view-transition/-/archive/v0.0.5/view-transition-v0.0.5.tar.gz"
    }
```
- npm install
- 导入转场动画css, 在globals.css里面导入，如果选择不导入此css文件，转场动画将默认淡入淡出方式，也可参考此css自定义转场动画
```bash
    @import url('~view-transition/dist/view-transition.css');
```


- 使用next/link
```bash
    import { AnimatedLink as Link } from 'view-transition';

    <Link href="/">
        {children}
    </Link>
```

- 与next路由配合使用
```bash
    import { useViewTransitionRouter as useRouter} from 'view-transition';

    const router = useRouter();
    const handleClick = () => {
        router.push(`/`);
    }
```

- 浏览器前进/后退全局监听，在根页面使用
```bash
    import { useViewTransition } from 'view-transition';

    const { popStateViewTransition } = useViewTransition();

    useEffect(() => {
        popStateViewTransition();
    }, []);
```
  
## Demo<!-- Required -->
<!-- 
* You can add a demo here GH supports images/ GIFs/videos 
* 
* It's recommended to use GIFs as they are more dynamic
-->


<div align="center">
    <img alt="demo" src="https://gitlab.com/gwave_mobile_public/jsfps/-/raw/Carrie.Li-main-patch-87553/demo.gif">
</div>




## Contributors<!-- Required -->
<!-- 
* Without contribution we wouldn't have open source. 
* 
* Generate github contributors Image here https://contrib.rocks/preview?repo=angular%2Fangular-ja
-->

<a href="https://gitlab.com/Carrie.Li">
  <img src="https://secure.gravatar.com/avatar/a559f0c99f2563b38137c89cbb25e0a3?s=40&d=identicon" />
</a>





<!-- Required -->
<!-- ### Contact -->
<!-- Reach me via email: [yousef_i44@protonmail.com](mailto:yousef_i44@protonmail.com) -->
<!-- 
* add your email and contact info here
* 
* 
-->
    
</details>

